#!/bin/bash

gcc -O3 -Wall ./source/*.c -o ./bin/dptstackvm.out

./bin/dptstackvm.out --assemble ./samples/fibonacci.dptasm --out ./bin/fibonacci.dpt
./bin/dptstackvm.out --assemble ./samples/exponentiation.dptasm --out ./bin/exponentation.dpt
./bin/dptstackvm.out --assemble ./samples/sort3.dptasm --out ./bin/sort3.dpt





