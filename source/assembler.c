/*
  DPTStackVM
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "base.h"


DEFINE_TYPEDEFS(FILE);

typedef struct Symbol
{
	Char name[256];
	U64  address;
} Symbol;

DEFINE_TYPEDEFS(Symbol);

typedef enum
{
	TOKEN_OP,
	TOKEN_NUMBER,
	TOKEN_NAME
} TokenType;

DEFINE_TYPEDEFS(TokenType);

typedef struct
{
	TokenType type;
	union
	{
		Operation op;
		S64       number;
		Char      name[256];
	};
} Token;

DEFINE_TYPEDEFS(Token);

#define SYMBOLS_LENGTH   1000
#define TOKENS_LENGTH   32768

static Symbol   g_symbols[SYMBOLS_LENGTH];
static Index    g_symbolsCount;
static Token    g_tokens[TOKENS_LENGTH];
static Index    g_tokensCount;
static CPC_Char g_operations[] = {
	[OP_NOP]      = "NOP",
	[OP_DUP]      = "DUP",
	[OP_COMPL]    = "COMPL",
	[OP_INC]      = "INC",
	[OP_DEC]      = "DEC",
	[OP_ADD]      = "ADD",
	[OP_SUB]      = "SUB",
	[OP_DIVREM]   = "DIVREM",
	[OP_MUL]      = "MUL",
	[OP_OR]       = "OR",
	[OP_XOR]      = "XOR",
	[OP_AND]      = "AND",
	[OP_NOT]      = "NOT",
	[OP_NEG]      = "NEG",
	[OP_DROPD]    = "DROPD",
	[OP_RETURN]   = "RETURN",
	[OP_SWAP]     = "SWAP",
	[OP_DTOC]     = "DTOC",
	[OP_CTOD]     = "CTOD",
	[OP_JUMPD]    = "JUMPD",
	[OP_JUMPDZ]   = "JUMPDZ",
	[OP_JUMPDNZ]  = "JUMPDNZ",
	[OP_JUMPDGZ]  = "JUMPDGZ",
	[OP_JUMPDLZ]  = "JUMPDLZ",
	[OP_JUMPDGEZ] = "JUMPDGEZ",
	[OP_JUMPDLEZ] = "JUMPDLEZ",
	[OP_ROLL3]    = "ROLL3",
	[OP_COMP]     = "COMP",
	[OP_CALLD]    = "CALLD",
	[OP_SYSCALL]  = "SYSCALL",
	[OP_STORED]   = "STORED",
	[OP_LOADD]    = "LOADD",
	[OP_EXIT]     = "EXIT",
	[OP_SHL]      = "SHL",
	[OP_SHR]      = "SHR",
	[OP_DROPC]    = "DROPC",
	[OP_LITD]     = "LITD",
	[OP_LITC]     = "LITC",
	[OP_LOOP]     = "LOOP",
	[OP_JUMP]     = "JUMP",
	[OP_JUMPZ]    = "JUMPZ",
	[OP_JUMPNZ]   = "JUMPNZ",
	[OP_JUMPGZ]   = "JUMPGZ",
	[OP_JUMPLZ]   = "JUMPLZ",
	[OP_JUMPGEZ]  = "JUMPGEZ",
	[OP_JUMPLEZ]  = "JUMPLEZ",
	[OP_CALL]     = "CALL",
	[OP_LOAD]     = "LOAD",
	[OP_STORE]    = "STORE",
	[OP_ENDENUM]  = NULL
};

static Error symbolsGetAddress (CPC_Char name,
				CP_U64   address)
{
	assert(name);
	assert(address);
	for (Index i = 0; i < g_symbolsCount; ++i)
	{
		if (!strcmp(g_symbols[i].name, name))
		{
			*address = g_symbols[i].address;
			return ERROR_NONE;
		}
	}
	return ERROR_UNKNOWN;
}

static Error symbolsAdd (CPC_Symbol symbol)
{
	assert(symbol);
	if (SYMBOLS_LENGTH <= g_symbolsCount + 1)
		return ERROR_MEMORY;
	for (Index i = 0; i < g_symbolsCount; ++i)
		if (!strcmp(g_symbols[i].name, symbol->name))
			return ERROR_COLLISION;
	g_symbols[g_symbolsCount++] = *symbol;
	return ERROR_NONE;
}

static Error toOperation (CPC_Char s, CP_Operation op)
{
	assert(s);
	assert(op);
	for (Index i = 0; i < OP_ENDENUM; ++i)
		if(g_operations[i] && !strcmp(g_operations[i], s))
		{
			*op = i;
			return ERROR_NONE;
		}
	return ERROR_UNKNOWN;
}

static Error toNumber (CPC_Char s, CP_S64 n)
{
	assert(s);
	assert(n);
        P_Char marker;
	*n = strtoll(s, &marker, 0);
	if (marker == s)
		return ERROR_UNKNOWN;
	return ERROR_NONE;
}

static Error toName (CPC_Char si, CP_Char so)
{
	assert(si);
	assert(so);
	if (':' == si[strlen(si) - 1])
		return ERROR_UNKNOWN;
	strcpy(so, si);
	return ERROR_NONE;
}

static Error toToken (CPC_Char word, CP_Token t)
{
	assert(word);
	assert(t);
	if (ERROR_NONE == toOperation(word, &t->op))
	{
		t->type = TOKEN_OP;
	}
	else if (ERROR_NONE == toNumber(word, &t->number))
	{
		t->type = TOKEN_NUMBER;
	}
	else if (ERROR_NONE == toName(word, t->name))
	{
		t->type = TOKEN_NAME;
	}
	else
		return ERROR_UNKNOWN;
	return ERROR_NONE;
}

static Error parse (CP_FILE f)
{
	assert(f);
	Boolean expectNumberOrName = false;
	Boolean expectOpOrSymbol   = true;
	Char    word[256];
	Char  currentMaster[256];
	Error catMaster (CP_Char s)
	{
		if ('\0' == currentMaster[0])
		{
			P_ERROR("Scoped SYMBOL %s has no parent\n", word);
			return ERROR_UNKNOWN;
		}
		strcat(s, currentMaster);
		return ERROR_NONE;
	}
	while (1 == fscanf(f, " %255s", word))
	{
		if (';' == word[0])
		{
			fscanf(f, "%*[^\n]\n");
			continue;
		}

		Token t;
		if (ERROR_NONE == toToken(word, &t))
		{
			switch (t.type)
			{
			case TOKEN_OP:
				if(!expectOpOrSymbol)
				{
					P_ERROR("Unexpected OP %s\n", word);
					return ERROR_UNKNOWN;	
				}
				if (64 <= t.op)
					expectOpOrSymbol   = false;
				expectNumberOrName = true;
				break;
			case TOKEN_NUMBER:
				if (!expectNumberOrName)
				{
					P_ERROR("Unexpected NUMBER %s\n",
						word);
					return ERROR_UNKNOWN;
				}
				expectOpOrSymbol   = true;
				expectNumberOrName = false;
				break;
			case TOKEN_NAME:
				if (!expectNumberOrName)
				{
					P_ERROR("Unexpected NAME %s\n", word);
					return ERROR_UNKNOWN;
				}
				if ('.' == t.name[0]
				    && ERROR_NONE != catMaster(t.name))
					return ERROR_UNKNOWN;
				expectOpOrSymbol   = true;
				expectNumberOrName = false;
				break;
			}
			g_tokens[g_tokensCount++] = t;
			continue;
		}
		if (!expectOpOrSymbol)
		{
			P_ERROR("Unexpected SYMBOL %s\n", word);
			return ERROR_UNKNOWN;
		}
	        word[strlen(word) - 1] = '\0';
		Char catWord[256];
		strcpy(catWord, word);
		if ('.' == catWord[0])
		{
			if (ERROR_NONE != catMaster(catWord))
				return ERROR_UNKNOWN;
		}
		else
			strcpy(currentMaster, word);
		Symbol sym = {
			.address = g_tokensCount
		};
		strcpy(sym.name, catWord);
		switch(symbolsAdd(&sym))
		{
		case ERROR_COLLISION:
			P_ERROR("SYMBOL %s already defined\n", word);
			return ERROR_COLLISION;
		case ERROR_MEMORY:
			P_ERROR("Out of memory,too many symbol\n");
			return ERROR_MEMORY;
		default:
			break;
		}
	}
	return ERROR_NONE;
}

/*
static Error printTokens (Void)
{
	for (Index i = 0; i < g_tokensCount; ++i)
	{
		printf("%010zu ", i);
		switch (g_tokens[i].type)
		{
		case TOKEN_OP:
			printf("    OP( %s )\n", g_operations[g_tokens[i].op]);
			break;
		case TOKEN_NUMBER:
			printf("NUMBER( %"PRId64" )\n", g_tokens[i].number);
			break;
		case TOKEN_NAME:
			printf("  NAME( %s )\n", g_tokens[i].name);
			break;
		}
	}
	return ERROR_NONE;
}
*/

static Error encode (CP_FILE f)
{
	assert(f);
	for (Index i = 0; i < g_tokensCount; ++i)
	{
		Length size    = 0;
		P_Void address = NULL;
		U64 sA;
		switch(g_tokens[i].type)
		{
		case TOKEN_OP:
			size    = sizeof(U8);
			address = &g_tokens[i].op;
			break;
		case TOKEN_NUMBER:
			size    = sizeof(U64);
			address = &g_tokens[i].number;
			break;
		case TOKEN_NAME:
			size    = sizeof(U64);
			if (ERROR_NONE != symbolsGetAddress(g_tokens[i].name,
							    &sA))
			{
				P_ERROR("Name %s not defined.\n",
					g_tokens[i].name);
				return ERROR_UNKNOWN;
			}
			address = &sA;
		}
		fwrite(address, size, 1, f);
	}
	return ERROR_NONE;
}

Error ASSEMBLER_assemble (CPC_Char filename, CPC_Char outFilename)
{
	assert(filename);
	assert(outFilename);
	CP_FILE in = fopen(filename, "r");
	if (!in)
	{
		P_ERROR("Could not open %s\n", filename);
		perror(NULL);
		return ERROR_FILE;
	}
	Error e = parse(in);
	fclose(in);
	if (ERROR_NONE != e)
	{
		P_ERROR("Failed to parse source file.");
		return e;
	}	
	//printTokens();
	CP_FILE out = fopen(outFilename, "wb");
	if (!out)
	{
		P_ERROR("Could not open %s\n", outFilename);
		perror(NULL);
		return ERROR_FILE;
	}
   	e = encode(out);
	fclose(out);
	if (ERROR_NONE != e)
	{
		P_ERROR("Failed to encode output file.\n");
		return e;
	}
	printf("%s assembled into %s\n", filename, outFilename);
	return ERROR_NONE;
}

Error ASSEMBLER_disassemble (CPC_Char filename)
{
	assert(filename);
	CP_FILE in = fopen(filename, "rb");
	if (!in)
	{
		P_ERROR("Could not open %s\n", filename);
		perror(NULL);
		return ERROR_FILE;
	}
	Index    ip = 0;
	Index cells = 0;
	while (!feof(in))
	{
		U8 op;
		if (0 == fread(&op, sizeof(U8), 1, in))
		    break;
		printf("0x%08"PRIu64" | %5zu | %s",
		       ip++,
		       cells++,
		       g_operations[op]);
		if (64 <= op)
		{
			U64 d;
			ip += 8;
			++cells;
			fread(&d, sizeof(U64), 1, in);
			printf(" %"PRId64, d);
		}
		puts("");
	}
	fclose(in);
	return ERROR_NONE;
}
