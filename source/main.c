/*
  DPTStackVM
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "base.h"

#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_REVISION 0
#define VERSION_STRING "1.0.0"

static Options g_Options = { 
	.imageFilename      = "program.dpt"
};

static Boolean g_assemble;
static Boolean g_disassemble;
static PC_Char g_outFile = "out.dpt";

static Void printVersion (void)
{
	puts(VERSION_STRING);
}

static Void printHeader (Void)
{
	puts("DPTStackVM v"VERSION_STRING" ("__DATE__")");
}

static Void printHelp (Void)
{
	printHeader();
	puts("\nUsage:\n"
	     "\tdptstackvm [options] [input filename]\n"
	     "\nOptions:\n"
	     "\t--help:        print this help\n"
	     "\t--verbose:     enable verbose mode\n"
	     "\t--version:     print version number\n"
	     "\t--assemble:    assemble a source file\n"
	     "\t--out:         output filename for assembler\n"
	     "\t--disassemble: disassemble an executable\n");	
}
/*
static Boolean isNumber (PC_Char string)
{
	assert(string);
	while (*string != '\0')
	{
		if (!isdigit(*(string++)))
			return false;
	}
	return true;
}
*/
static Error parseCommandLine (C_Length length, CPCPC_Char line)
{
	assert(length > 0);
	assert(line);	
	for (U32 i = 1; i < length; ++i)
	{
		if (!strcmp("--help", line[i]))
		{
			printHelp();
			exit(0);
		}
		else if (!strcmp("--verbose", line[i]))
			g_Options.verboseMode = true;
		else if (!strcmp("--version", line[i]))
			printVersion();
		else if (!strcmp("--assemble", line[i]))
			g_assemble = true;
		else if (!strcmp("--disassemble", line[i]))
			g_disassemble = true;
		else if (!strcmp("--out", line[i]))
		{
			++i;
			if (i >= length)
			{
			        P_ERROR("Missing value after option --out\n");
				return ERROR_BAD_OPTION;
			}			
			g_outFile = line[i];
		}
		else
		{
			g_Options.imageFilename = line[i];
		}
	}
	if (g_assemble && g_disassemble)
	{
		P_ERROR("Can't mix --assemble and --disassemble\n");
		return ERROR_BAD_OPTION;
	}
	return ERROR_NONE;
}

int main (Integer argc, CPCPC_Char argv)
{
	if (ERROR_NONE != parseCommandLine(argc, argv))
		return 0;
	Error e = ERROR_NONE;
	if (g_assemble)
		e = ASSEMBLER_assemble(g_Options.imageFilename, g_outFile);
	else if(g_disassemble)
		e = ASSEMBLER_disassemble(g_Options.imageFilename);
	else
		e = VM_start(&g_Options);
	if (ERROR_NONE != e)
		printf("ERROR %u\n", e);
}
