/*
  DPTStackVM
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdio.h>
#include "base.h"

DEFINE_TYPEDEFS(FILE);

#define PROGRAM_LENGTH     8192 // 64 KiB
#define HEAP_LENGTH    16777216 // 16 MiB
#define DSTACK_LENGTH       256 //  2 KiB
#define CSTACK_LENGTH       128 //  1 KiB

static   Boolean g_verbose;
static    Length g_programLength;
static       S64 g_heap[HEAP_LENGTH];
static CallStack g_cStack[CSTACK_LENGTH];
static   Program g_program[PROGRAM_LENGTH + 1];
static DataStack g_dStack[DSTACK_LENGTH];

static Error predecode (CPCP_Void iopAddresses)
{
	for (Index i = 0; i < g_programLength; ++i)
	{
		C_Operation op = g_program[i].u64;
		g_program[i].pV = iopAddresses[op];
		if (64 <= op)
			++i;
	}
	return ERROR_NONE;
}

/*
 * Predecode and execute the bytecode.
 */
static Error execute (Void)
{
	P_Program   restrict ip     = g_program;
	P_DataStack restrict dStack = g_dStack;
	P_CallStack restrict cStack = g_cStack;
	//set end marker.
	g_program[g_programLength].pV = &&L_OP_EXIT;
	CP_Void iopAddresses[] = {
		[OP_NOP]      = &&L_OP_NOP,
		[OP_DUP]      = &&L_OP_DUP,
		[OP_COMPL]    = &&L_OP_COMPL,
		[OP_INC]      = &&L_OP_INC,
		[OP_DEC]      = &&L_OP_DEC,
		[OP_ADD]      = &&L_OP_ADD,
		[OP_SUB]      = &&L_OP_SUB,
		[OP_DIVREM]   = &&L_OP_DIVREM,
		[OP_MUL]      = &&L_OP_MUL,
		[OP_OR]       = &&L_OP_OR,
		[OP_XOR]      = &&L_OP_XOR,
		[OP_AND]      = &&L_OP_AND,
		[OP_NOT]      = &&L_OP_NOT,
		[OP_NEG]      = &&L_OP_NEG,
		[OP_DROPD]    = &&L_OP_DROPD,
		[OP_RETURN]   = &&L_OP_RETURN,
		[OP_SWAP]     = &&L_OP_SWAP,
		[OP_DTOC]     = &&L_OP_DTOC,
		[OP_CTOD]     = &&L_OP_CTOD,
		[OP_JUMPD]    = &&L_OP_JUMPD,
		[OP_JUMPDZ]   = &&L_OP_JUMPDZ,
		[OP_JUMPDNZ]  = &&L_OP_JUMPDNZ,
		[OP_JUMPDGZ]  = &&L_OP_JUMPDGZ,
		[OP_JUMPDLZ]  = &&L_OP_JUMPDLZ,
		[OP_JUMPDGEZ] = &&L_OP_JUMPDGEZ,
		[OP_JUMPDLEZ] = &&L_OP_JUMPDLEZ,
		[OP_ROLL3]    = &&L_OP_ROLL3,
		[OP_COMP]     = &&L_OP_COMP,
		[OP_CALLD]    = &&L_OP_CALLD,
		[OP_SYSCALL]  = &&L_OP_SYSCALL,
		[OP_STORED]   = &&L_OP_STORED,
		[OP_LOADD]    = &&L_OP_LOADD,
		[OP_EXIT]     = &&L_OP_EXIT,
		[OP_SHL]      = &&L_OP_SHL,
		[OP_SHR]      = &&L_OP_SHR,
		[OP_DROPC]    = &&L_OP_DROPC,
		[OP_LITD]     = &&L_OP_LITD,
		[OP_LITC]     = &&L_OP_LITC,
		[OP_LOOP]     = &&L_OP_LOOP,
		[OP_JUMP]     = &&L_OP_JUMP,
		[OP_JUMPZ]    = &&L_OP_JUMPZ,
		[OP_JUMPNZ]   = &&L_OP_JUMPNZ,
		[OP_JUMPGZ]   = &&L_OP_JUMPGZ,
		[OP_JUMPLZ]   = &&L_OP_JUMPLZ,
		[OP_JUMPGEZ]  = &&L_OP_JUMPGEZ,
		[OP_JUMPLEZ]  = &&L_OP_JUMPLEZ,
		[OP_CALL]     = &&L_OP_CALL,
		[OP_LOAD]     = &&L_OP_LOAD,
		[OP_STORE]    = &&L_OP_STORE
	};
	predecode(iopAddresses);
	goto L_NEXT_OP;
L_INC_IP:
	++ip;
L_NEXT_OP:
	goto *(ip->pV);
L_OP_NOP:
	goto L_INC_IP;
L_OP_DUP:
	{
		C_S64 t = (dStack++)->s64;
		dStack->s64 = t;
	}
	goto L_INC_IP;
L_OP_COMPL:
	dStack->s64 = ~dStack->s64;
	goto L_INC_IP;
L_OP_INC:
	++(dStack->s64);
	goto L_INC_IP;
L_OP_DEC:
	--(dStack->s64);
	goto L_INC_IP;
L_OP_ADD:
	{
		C_S64 t = (dStack--)->s64;
		dStack->s64 += t;
	}
	goto L_INC_IP;
L_OP_SUB:
	{
		C_S64 t = (dStack--)->s64;
		dStack->s64 -= t;
	}
	goto L_INC_IP;
L_OP_DIVREM:
	{
		C_S64 dividend = dStack->s64;
		C_S64 divisor  = (dStack - 1)->s64;
		dStack->s64 = dividend % divisor;
		(dStack - 1)->s64 = dividend  / divisor; 
	}
	goto L_INC_IP;
L_OP_MUL:
	{
		C_S64 multiplicand = (dStack--)->s64;
		dStack->s64 *= multiplicand;
	}
	goto L_INC_IP;
L_OP_OR:
	{
		C_S64 t = (dStack--)->s64;
		dStack->s64 |= t;
	}
	goto L_INC_IP;
L_OP_XOR:
	{
		C_S64 t = (dStack--)->s64;
		dStack->s64 ^= t;
	}
	goto L_INC_IP;
L_OP_AND:
	{
		C_S64 t = (dStack--)->s64;
		dStack->s64 &= t;
	}
	goto L_INC_IP;
L_OP_NOT:
	dStack->s64 = ~dStack->s64;
	goto L_INC_IP;
L_OP_NEG:
	dStack->s64 = -dStack->s64;
	goto L_INC_IP;
L_OP_DROPD:
	--dStack;
	goto L_INC_IP;
L_OP_RETURN:
	ip = g_program + *(cStack--);
	goto L_NEXT_OP;
L_OP_SWAP:
	{
		C_S64 t = dStack->s64;
		dStack->s64 = (dStack - 1)->s64;
		(dStack - 1)->s64 = t;
	}
	goto L_INC_IP;
L_OP_DTOC:
	*(++cStack) = (dStack--)->u64;
	goto L_INC_IP;
L_OP_CTOD:
	(++dStack)->u64 = *(cStack--);
	goto L_INC_IP;
L_OP_JUMPD:
	ip = g_program + (dStack--)->u64;
	goto L_NEXT_OP;
L_OP_JUMPDZ:
	{
		C_U64 address = (dStack--)->u64;
		if (0 == (dStack--)->u64)
			ip = g_program + address;
	}
	goto L_NEXT_OP;
L_OP_JUMPDNZ:
	{
		C_U64 address = (dStack--)->u64;
		if (0 != (dStack--)->u64)
			ip = g_program + address;
	}
	goto L_NEXT_OP;
L_OP_JUMPDGZ:
	{
		C_U64 address = (dStack--)->u64;
		if (0 < (dStack--)->u64)
			ip = g_program + address;
	}
	goto L_NEXT_OP;
L_OP_JUMPDLZ:
	{
		C_U64 address = (dStack--)->u64;
		if (0 > (dStack--)->u64)
			ip = g_program + address;
	}
	goto L_NEXT_OP;
L_OP_JUMPDGEZ:
	{
		C_U64 address = (dStack--)->u64;
		if (0 <= (dStack--)->u64)
			ip = g_program + address;
	}
	goto L_NEXT_OP;
L_OP_JUMPDLEZ:
	{
		C_U64 address = (dStack--)->u64;
		if (0 >= (dStack--)->u64)
			ip = g_program + address;
	}
	goto L_NEXT_OP;
L_OP_ROLL3:
	{
		C_S64 temp = dStack->s64;
		dStack->s64 = (dStack - 1)->s64;
		(dStack - 1)->s64 = (dStack - 2)->s64;
		(dStack - 2)->s64 = temp;
	}
	goto L_INC_IP;
L_OP_COMP:
	{
		C_S64 res = (dStack - 1)->s64 - dStack->s64;
		(++dStack)->s64 = res;
	}
	goto L_INC_IP;
L_OP_CALLD:
	{
		*(++cStack) = (ip - g_program) + 1;
		ip = g_program + (dStack--)->u64;
	}
	goto L_NEXT_OP;
L_OP_SYSCALL:
        switch ((dStack--)->u64)
	{
	case SYSPROC_PRINTNUMBER:
		printf("%"PRId64, (dStack--)->s64);
		break;
	case SYSPROC_PRINTNUMBERNL:
		printf("%"PRId64"\n", (dStack--)->s64);
		break;
	case SYSPROC_PRINTNEWLINE:
		puts("");
	case SYSPROC_READNUMBER:
		scanf("%"PRId64, &(++dStack)->s64);
		break;
	case SYSPROC_PRINTSTRING:
		printf("%s", (CPC_Char)&g_heap[(dStack--)->u64]);
		break;
	default:	
		break;
	}
	goto L_INC_IP;
L_OP_STORED:
	{
		C_U64 address = (dStack--)->u64;
		g_heap[address] = (dStack--)->s64;
	}
	goto L_INC_IP;
L_OP_LOADD:
	dStack->s64 = g_heap[dStack->s64];
	goto L_INC_IP;
L_OP_EXIT:
	goto L_EXIT;
L_OP_SHL:
	{
		C_U64 c = (dStack--)->u64;
		dStack->u64 <<= c;
	}
	goto L_INC_IP;
L_OP_SHR:
	{
		C_U64 c = (dStack--)->u64;
		dStack->u64 >>= c;
	}
	goto L_INC_IP;
L_OP_DROPC:
	--cStack;
	goto L_INC_IP;
L_OP_LITD:
	++ip;
	(++dStack)->s64 = ip->s64;
	goto L_INC_IP;
L_OP_LITC:
	++ip;
	*(++cStack) = ip->u64;
	goto L_INC_IP;
L_OP_LOOP:
	++ip;
	if (0 < --(*cStack))
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	--cStack;
	goto L_INC_IP;
L_OP_JUMP:
	{
		C_U64 offset = (++ip)->u64;
		ip = g_program + offset;
	}
	goto L_NEXT_OP;
L_OP_JUMPZ:
	++ip;
	if (0 == (dStack--)->s64)
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	goto L_INC_IP;
L_OP_JUMPNZ:
	++ip;
	if (0 != (dStack--)->s64)
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	goto L_INC_IP;
L_OP_JUMPGZ:
	++ip;
	if (0 < (dStack--)->s64)
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	goto L_INC_IP;
L_OP_JUMPLZ:
	++ip;
	if (0 >= (dStack--)->s64)
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	goto L_INC_IP;
L_OP_JUMPGEZ:
	++ip;
	if (0 <= (dStack--)->s64)
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	goto L_INC_IP;
L_OP_JUMPLEZ:
	++ip;
	if (0 >= (dStack--)->s64)
	{
		ip = g_program + ip->u64;
		goto L_NEXT_OP;
	}
	goto L_INC_IP;
L_OP_CALL:
	++ip;
	*(++cStack) = (ip - g_program) + 1;
	ip = g_program + ip->u64;
	goto L_NEXT_OP;
L_OP_LOAD:
	++ip;
	(++dStack)->s64 = g_heap[ip->u64];
	goto L_INC_IP;
L_OP_STORE:
	++ip;
	g_heap[ip->u64] = (dStack--)->s64;
	goto L_INC_IP;
L_EXIT:
	if (g_verbose)
	{
		puts("-- EXECUTION END --\n"
		     "Data stack:");
		while (g_dStack != dStack)
			printf("\t[%20"PRId64"]\n", (dStack--)->s64);
	}
	
	return ERROR_NONE;
}

/*
 * Load program's instructions, convert them to 64 bits cells.
 */
static Error loadProgram (CPC_Char filename)
{
	assert(filename);
	P_U8 bufferMemory = malloc(PROGRAM_LENGTH);
	if (!bufferMemory)
		return ERROR_MEMORY;
	CP_FILE fp = fopen(filename, "rb");
	if (!fp)
	{
		free(bufferMemory);
		P_ERROR("Could not open %s\n", filename);
		perror(NULL);
		return ERROR_FILE;
	}
	C_Length fileLength = fread(bufferMemory,
				    sizeof(U8),
				    PROGRAM_LENGTH,
				    fp);
	fclose(fp);
	Index i = 0;
	Index insIndex = 0;
		union {
		P_U8 pU8;
		P_U64 pU64;
	} buffer = {
		.pU8 = bufferMemory
	};
	while (fileLength > insIndex)
	{
		C_Operation op = *(buffer.pU8++);
		g_program[i++].u64 = op;
		++insIndex;
		if (64 <= op)
		{
			g_program[i++].u64 = *(buffer.pU64++);
			insIndex += 8;
		}
	}
        g_programLength = i;
	free(bufferMemory);	
	return ERROR_NONE;
}


/*
 * Set options, load program, and execute.
 */
Error VM_start (CPC_Options opts)
{
	assert(opts);
	g_verbose = opts->verboseMode;
	if (g_verbose)
		printf("Verbose mode activated\n"
		       "Loading %s...",
		       opts->imageFilename);
	C_Error e = loadProgram(opts->imageFilename);	
	if (ERROR_NONE != e)
		return e;
	if (g_verbose)
		printf("Loaded.\n"
		       "Program's length: %"PRIu64"\n"
		       "-- EXECUTION START --\n",
		       g_programLength);
	return execute();
} 
			
