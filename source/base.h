#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#define DEFINE_TYPEDEFS(Type)				\
	typedef Type const C_## Type;			\
	typedef Type *P_## Type;			\
	typedef Type const *PC_## Type;			\
	typedef Type * const CP_## Type;		\
	typedef Type const * const CPC_## Type;		\
	typedef Type **PP_## Type;			\
	typedef Type const **PPC_## Type;		\
	typedef Type ** const CPP_## Type;		\
	typedef Type * const * PCP_## Type;		\
	typedef Type * const * const CPCP_## Type;	\
	typedef Type const * const * PCPC_## Type;	\
	typedef Type const ** const CPPC_## Type;	\
	typedef Type const * const * const CPCPC_## Type;

typedef           int Integer;
typedef  unsigned int UInteger;
DEFINE_TYPEDEFS(Integer);
DEFINE_TYPEDEFS(UInteger);

typedef   int8_t  S8;
typedef  int16_t S16;
typedef  int32_t S32;
typedef  int64_t S64;

typedef  uint8_t  U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;

typedef    float R32;
typedef   double R64;

typedef   size_t Size;
typedef   size_t Index;

typedef     char Char;
typedef     void Void;
typedef     bool Boolean;

DEFINE_TYPEDEFS(S8);
DEFINE_TYPEDEFS(S16);
DEFINE_TYPEDEFS(S32);
DEFINE_TYPEDEFS(S64);

DEFINE_TYPEDEFS(U8);
DEFINE_TYPEDEFS(U16);
DEFINE_TYPEDEFS(U32);
DEFINE_TYPEDEFS(U64);

DEFINE_TYPEDEFS(R32);
DEFINE_TYPEDEFS(R64);

DEFINE_TYPEDEFS(Size);
DEFINE_TYPEDEFS(Index);

DEFINE_TYPEDEFS(Char);
DEFINE_TYPEDEFS(Void);
DEFINE_TYPEDEFS(Boolean);


typedef  S8  VectorS8 __attribute__ ((vector_size (16)));
typedef S16 VectorS16 __attribute__ ((vector_size (16)));
typedef S32 VectorS32 __attribute__ ((vector_size (16)));
typedef S64 VectorS64 __attribute__ ((vector_size (16)));

typedef  U8  VectorU8 __attribute__ ((vector_size (16)));
typedef U16 VectorU16 __attribute__ ((vector_size (16)));
typedef U32 VectorU32 __attribute__ ((vector_size (16)));
typedef U64 VectorU64 __attribute__ ((vector_size (16)));

typedef R32 VectorR32 __attribute__ ((vector_size (16)));
typedef R64 VectorR64 __attribute__ ((vector_size (16)));


DEFINE_TYPEDEFS(VectorS8);
DEFINE_TYPEDEFS(VectorS16);
DEFINE_TYPEDEFS(VectorS32);
DEFINE_TYPEDEFS(VectorS64);

DEFINE_TYPEDEFS(VectorU8);
DEFINE_TYPEDEFS(VectorU16);
DEFINE_TYPEDEFS(VectorU32);
DEFINE_TYPEDEFS(VectorU64);

DEFINE_TYPEDEFS(VectorR32);
DEFINE_TYPEDEFS(VectorR64);

typedef enum
{
	ERROR_NONE,
	ERROR_UNKNOWN,
	ERROR_MEMORY,
	ERROR_FILE,
	ERROR_LOAD_PROGRAM,
	ERROR_NOT_IMPLEMENTED,
	ERROR_BAD_OPTION,
	ERROR_COLLISION
} Error;

DEFINE_TYPEDEFS(Error);

typedef Size Length;

DEFINE_TYPEDEFS(Length);

#define P_ERROR(msg, ...) fprintf(stderr, "ERROR: "msg, ##__VA_ARGS__)

typedef struct
{
	Boolean  verboseMode;
	PC_Char  imageFilename;
} Options;

DEFINE_TYPEDEFS(Options);

typedef union
{
	U8     u8;
	U64    u64;
	S64    s64;
	P_Void pV;
} Program;

DEFINE_TYPEDEFS(Program);

typedef enum
{
	OP_NOP,
	OP_DUP,
	OP_COMPL,
	OP_INC,
	OP_DEC,
	OP_ADD,
	OP_SUB,
	OP_DIVREM,
	OP_MUL,
	OP_OR,
	OP_XOR,
	OP_AND,
	OP_NOT,
	OP_NEG,
	OP_DROPD,
	OP_RETURN,
	OP_SWAP,
	OP_DTOC,
	OP_CTOD,
	OP_JUMPD,
	OP_JUMPDZ,
	OP_JUMPDNZ,
	OP_JUMPDGZ,
	OP_JUMPDLZ,
	OP_JUMPDGEZ,
	OP_JUMPDLEZ,
	OP_ROLL3,
	OP_COMP,
	OP_CALLD,
	OP_SYSCALL,
	OP_STORED,
	OP_LOADD,
	OP_EXIT,
	OP_SHL,
	OP_SHR,
	OP_DROPC,
	OP_LITD       = 64,
	OP_LITC,
	OP_LOOP,
	OP_JUMP,
	OP_JUMPZ,
	OP_JUMPNZ,
	OP_JUMPGZ,
	OP_JUMPLZ,
	OP_JUMPGEZ,
	OP_JUMPLEZ,
	OP_CALL,
	OP_LOAD,
	OP_STORE,
	OP_ENDENUM
} Operation;

DEFINE_TYPEDEFS(Operation);

typedef enum
{
	SYSPROC_PRINTNUMBER,
	SYSPROC_PRINTNUMBERNL,
	SYSPROC_PRINTNEWLINE,
	SYSPROC_READNUMBER,
	SYSPROC_PRINTSTRING,
	SYSPROC_ENDENUM
} SystemProcedureId;

DEFINE_TYPEDEFS(SystemProcedureId);

typedef P_Void OperationsAddressTable[OP_ENDENUM];

DEFINE_TYPEDEFS(OperationsAddressTable);

typedef union
{
	S64 s64;
	U64 u64;
} DataStack;

DEFINE_TYPEDEFS(DataStack);

typedef U64 CallStack;

DEFINE_TYPEDEFS(CallStack);

Error VM_start (CPC_Options opts);
Error ASSEMBLER_assemble (CPC_Char filename, CPC_Char outFilename);
Error ASSEMBLER_disassemble (CPC_Char filename);
