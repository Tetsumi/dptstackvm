
DPTStackVM
=====================

A virtual stack machine composed of two stacks: a data stack and a call stack.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)



Lexicon
---------------------

 Term  | Definition                   
-------|----------------------------  
 TODS  | Top value of data stack. 
 TOCS  | Top value of call stack.
 NODS  | Next value on data stack.
 NOCS  | Next value on call stack.
DSTACK | Data stack.
CSTACK | Call stack.
PUSHD  | Push value on data stack.
PUSHC  | Push value on call stack.
POPD   | Pop value from data sack.
POPC   | Pop value from call stack.
IP     | Instruction pointer.  



Memory
---------------------

Size of a Cell is 64 bits.  

 Name    | Size              | Usage
---------|------------------:|----------------------
 R0      |           1  Cell | Temporary value register.  
 R1      |           1  Cell | Temporary value register.
 R2      |           1  Cell | Temporary value register.
 IP      |           1  Cell | Instruction Pointer.
 DSTACK  |      >= 256 Cells | Data stack.
 CSTACK  |      >= 128 Cells | Call stack.
 HEAP    | >= 16777216 Cells | Heap.
 PROGRAM |     >= 8192 Cells | Program's code.

An implementation may ignore the RX registers, those are used in the pseudo code.

Executable Format
---------------------
An executable is nothing more than a binary file containing instructions. The
instructions are Byte sized while values remain Cell sized.

For example, the following program in memory

```
0 | [0x0000000000000040] | LITD
1 | [0x00000000ffeeddcc] | 4293844428
2 | [0x0000000000000040] | LITD
3 | [0x0000000088776655] | 2289526357
4 | [0x0000000000000006] | SUB
```

shall be compressed into

```
0 |               [0x40] | LITD
1 | [0x00000000ffeeddcc] | 4293844428
2 |               [0x40] | LITD
3 | [0x0000000088776655] | 2289526357
4 |               [0x06] | SUB
```

Instructions
---------------------

Code is in decimal. Some instructions may be followed by a 64 bits value (Any
instruction with a code >= 64).

Codes [0, 128] are reserved.

```
+==========+======+=========================================+=========================+
| Mnemonic | Code |Description                              | Pseudo code             |
+==========+======+=========================================+=========================+
| NOP      |  000 |Do nothing (may be used for alignment).  | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| DUP      |  001 |Pop value then push it two times to      | POPD  R0                |
|          |      |duplicate it.                            | PUSHD R0                |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| COMPL    |  002 |Pop value then push its complement       | POPD R0                 |
|          |      |(logical negation).                      | R0 := !R0               |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| INC      |  003 |Pop value, increment, and push the       | POPD  R0                |
|          |      |result.                                  | R0 := R0 + 1            |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| DEC      |  004 |Pop value, decrement, and push the       | POPD  R0                |
|          |      |result.                                  | R0 := R0 - 1            |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| ADD      |  005 |Pop two addends, add, and push the       | POPD  R0                |
|          |      |result.                                  | POPD  R1                |
|          |      |                                         | R1 := R1 + R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| SUB      |  006 |Pop subtrahend and minuend, subtract, and| POPD  R0                |
|          |      |push the result.                         | POPD  R1                |
|          |      |                                         | R1 := R1 - R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| DIVREM   |  007 |Pop the dividend and the divisor, divide,| POPD R0                 |
|          |      |push the quotient and the reminder.      | POPD R1                 |
|          |      |                                         | R2 := R0 / R1           | 
|          |      |                                         | R1 := R0 % R1           |
|          |      |                                         | PUSHD R2                |	 
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| MUL      |  008 |Pop the multiplicand and the multiplier, | POPD  R0                |
|          |      |and multiply, push the result.           | POPD  R1                |
|          |      |                                         | R1 := R1 * R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| OR       |  009 |Pop two values, bitwise OR, and push the | POPD  R0                |
|          |      |result.                                  | POPD  R1                |
|          |      |                                         | R1 := R1 | R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| XOR      |  010 |Pop two values, bitwise XOR, and push the| POPD  R0                |
|          |      |result.                                  | POPD  R1                |
|          |      |                                         | R1 := R1 ^ R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| AND      |  011 |Pop values, bitwise AND, and push the    | POPD  R0                |
|          |      |result.                                  | POPD  R1                |
|          |      |                                         | R1 := R1 & R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| NOT      |  012 |Pop value, bitwise NOT, and push the     | POPD R0                 |
|          |      |result.                                  | R0 := ~R0               |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| NEG      |  013 |Pop value, negetate, and push the result.| POPD  R0                |
|          |      |                                         | R0 := -R0               |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| DROPD    |  014 |Discard top value of data stack.         | POPD  R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| RETURN   |  015 |Pop value of CSTACK into IP.             | POPC IP                 |
+----------+------+-----------------------------------------+-------------------------+
| SWAP     |  016 |Permute the top two values.              | POPD  R0                |
|          |      |                                         | POPD  R1                |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| DTOC     |  017 |Pop a value of DSTACK then push on       | POPD  R0                |
|          |      |CSTACK.                                  | PUSHC R0                |
|          |      |                                         | IP := IP + 1            | 
+----------+------+-----------------------------------------+-------------------------+
| CTOD     |  018 |Pop a value of CSTACK then push on       | POPC  R0                |
|          |      |DSTACK.                                  | PUSHD R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| JUMPD    |  019 |Pop top value into IP.                   | POPD  IP                |
+----------+------+-----------------------------------------+-------------------------+
| JUMPDZ   |  020 |Pop two values, if second value is equal | POPD R0                 |
|          |      |to zero, set IP to first value.          | POPD R1                 |
|          |      |Otherwise, continue.                     | IF R1 = 0 THEN          |
|          |      |                                         |     IP := R0            |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 1        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPDNZ  |  021 |Pop two values, if second value is not   | POPD R0                 |
|          |      |equal to zero, set IP to first value.    | POPD R1                 |
|          |      |Otherwise, continue.                     | IF R1 != 0 THEN         |
|          |      |                                         |     IP := R0            |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 1        |
|          |      |                                         | ENDIF                   |	
+----------+------+-----------------------------------------+-------------------------+
| JUMPDGZ  |  022 |Pop two values, if second value is       | POPD R0                 |
|          |      |greater than zero, set IP to first value.| POPD R1                 |
|          |      |Otherwise, continue.                     | IF R1 > 0 THEN          |
|          |      |                                         |     IP := R0            |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 1        |
|          |      |                                         | ENDIF                   |	
+----------+------+-----------------------------------------+-------------------------+
| JUMPDLZ  |  023 |Pop two values, if second value is less  | POPD R0                 |
|          |      |than zero, set IP to first value.        | POPD R1                 |
|          |      |Otherwise, continue.                     | IF R1 < 0 THEN          |
|          |      |                                         |     IP := R0            |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 1        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPDGEZ |  024 |Pop two values, if second value is       | POPD R0                 |
|          |      |greater or equal to 0, set IP to first   | POPD R1                 |
|          |      |value. Otherwise, continue.              | IF R1 >= 0 THEN         |
|          |      |                                         |     IP := R0            |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 1        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPDLEZ |  025 |Pop two values, if second value is less  | POPD R0                 |
|          |      |or equal to 0, set IP to first value.    | POPD R1                 |
|          |      |Otherwise, continue.                     | IF R1 <= 0 THEN         |
|          |      |                                         |     IP := R0            |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 1        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| ROLL3    |  026 |Cyclically permute the top three values. | POPD  R0                |
|          |      |                                         | POPD  R1                |
|          |      |                                         | POPD  R2                |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | PUSHD R2                |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| COMP     |  027 |Pop subtrahend and minuend, subtract,    | POPD R0                 |
|          |      |push back minued, subtrahend, and result.| POPD R1                 |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | R1 := R1 - R0           |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| CALLD    |  028 |Push return address on CSTACK and pop    | IP := IP + 1            |
|          |      |a value into IP.                         | PUSHC IP                |
|          |      |                                         | POPD IP                 |
+----------+------+-----------------------------------------+-------------------------+
| SYSCALL  |  029 |Call a system procedure. Top value on    |                         |
|          |      |DSTACK is the procedure ID, following    |                         |
|          |      |values are the procedure arguments.      |                         |
+----------+------+-----------------------------------------+-------------------------+
| STORED   |  030 |Pop heap address, pop value, and store   | POPD R0                 |
|          |      |value at the given address.              | POPD R1                 |
|          |      |                                         | HEAP[R0] := R1          |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| LOADD    |  031 |Pop heap address and push value from the | POPD R0                 |
|          |      |given address.                           | R1 := HEAP[R0]          |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| EXIT     |  032 |Pop exit code and end execution of the   | POPD R0                 |
|          |      |program.                                 | EXIT R0                 |
+----------+------+-----------------------------------------+-------------------------+
| SHL      |  033 |Pop two values, shift the second value to| POPD R0                 |
|          |      |the left for a count specified by the    | POPD R1                 |
|          |      |first value.                             | R1 := R1 << R0          |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| SHR      |  034 |Pop two values, shift the second value to| POPD R0                 |
|          |      |the right for a count specified by the   | POPD R1                 |
|          |      |first value.                             | R1 := R1 >> R0          |
|          |      |                                         | PUSHD R1                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| DROPC    |  035 |Discard top value of call stack.         | POPC  R0                |
|          |      |                                         | IP := IP + 1            |
+----------+------+-----------------------------------------+-------------------------+
| LITD     |  064 |Push a 64 bits value following the       | IP := IP + 1            |
|          |      |instruction on DSTACK.                   | R0 := PROGRAM[IP]       |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 8            |
+----------+------+-----------------------------------------+-------------------------+
| LITC     |  065 |Push a 64 bits value following the       | IP := IP + 1            |
|          |      |instruction on CSTACK.                   | R0 := PROGRAM[IP]       |
|          |      |                                         | PUSHC R0                |
|          |      |                                         | IP := IP + 8            |
+----------+------+-----------------------------------------+-------------------------+
| LOOP     |  066 |Pop a value from call stack, decrement,  | POPD  R0                |
|          |      |if result is greater than zero, push     | R0 := R0 - 1            |
|          |      |result on call stack and set IP to a 64  | IP := IP + 1            |
|          |      |bits value following the instruction. If | IF R0 >= 0 THEN         |
|          |      |the result is equal to zero, then        |     PUSHD R0            |
|          |      |continue without taking the jump.        |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMP     |  067 |Set IP to a 64 bits value following the  | IP := IP + 1            |
|          |      |instruction.                             | IP := PROGRAM[IP]       |
+----------+------+-----------------------------------------+-------------------------+
| JUMPZ    |  068 |Pop a value, if that value is equal to   | POPD R0                 |
|          |      |zero, set IP to a 64 bits value following| IP := IP + 1            |
|          |      |the instruction. Otherwise, continue.    | IF R0 = 0 THEN          |
|          |      |                                         |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPNZ   |  069 |Pop a value, if that value is not equal  | POPD R0                 |
|          |      |to zero, set IP to a 64 bits value       | IP := IP + 1            |
|          |      |following the instruction. Otherwise,    | IF R0 != 0 THEN         |
|          |      |continue.                                |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPGZ   |  070 |Pop a value, if that value is greater    | POPD R0                 |
|          |      |than zero, set IP to a 64 bits value     | IP := IP + 1            |
|          |      |following the instruction. Otherwise,    | IF R0 > 0 THEN          |
|          |      |continue.                                |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPLZ   |  071 |Pop a value, if that value is less than  | POPD R0                 |
|          |      |zero, set IP to a 64 bits value following| IP := IP + 1            |
|          |      |the instruction. Otherwise, continue.    | IF R0 < 0 THEN          |
|          |      |                                         |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPGEZ  |  072 |Pop a value, if that value is greater or | POPD R0                 |
|          |      |equal to zero, set IP to a 64 bits value | IP := IP + 1            |
|          |      |following the instruction. Otherwise,    | IF R0 >= 0 THEN         |
|          |      |continue.                                |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| JUMPLEZ  |  073 |Pop a value, if that value is less or    | POPD R0                 |
|          |      |equal to zero, set IP to a 64 bits value | IP := IP + 1            |
|          |      |following the instruction. Otherwise,    | IF R0 <= 0 THEN         |
|          |      |continue.                                |     IP := PROGRAM[IP]   |
|          |      |                                         | ELSE                    |
|          |      |                                         |     IP := IP + 8        |
|          |      |                                         | ENDIF                   |
+----------+------+-----------------------------------------+-------------------------+
| CALL     |  074 |Set IP to a 64 bits value following the  | IP := IP + 1            |
|          |      |instruction then push the return address | R0 := IP                |
|          |      |on the CSTACK.                           | IP := PROGRAM[IP]       |
|          |      |                                         | R0 := R0 + 8            |
|          |      |                                         | PUSHC R0                |
+----------+------+-----------------------------------------+-------------------------+
| LOAD     |  075 |Load a value at the 64 bits address      | IP := IP + 1            |
|          |      |following the instruction and push that  | R0 := PROGRAM[IP]       |
|          |      |value on DSTACK.                         | R0 := HEAP[R0]          |
|          |      |                                         | PUSHD R0                |
|          |      |                                         | IP := IP + 8            |
+----------+------+-----------------------------------------+-------------------------+
| STORE    |  076 |Pop a value and store it at the 64 bits  | POPD R0                 |
|          |      |address following the instruction.       | IP := IP + 1            |
|          |      |                                         | R1 := PROGRAM[IP]       |
|          |      |                                         | HEAP[R1] := R0          |
|          |      |                                         | IP := IP + 8            |
+==========+======+=========================================+=========================+
```


System Procedures
---------------------

ID is in decimal. IDs [0, 511] are reserved.

 Name           | ID | Arguments | Description
----------------|---:|----------:|----------------------------------------------------------------
 printNumber    |  0 |         1 | Pop a number and print it to stdout.
 printNumberNl  |  1 |         1 | Pop a number, print it to stdout, and terminate current line.
 printNewline   |  2 |         0 | Terminate current stdout's line.
 readNumber     |  3 |         0 | Read number from stdin and push it.
 printString    |  4 |         1 | Pop a heap address and print string to stdout.



To Do
---------------------

Need more system procedures for

- output
- input
- files
- time / date
- random
- environment (stacks length, heap length, ...)

Need more instructions for

- real numbers
- self modifying code
